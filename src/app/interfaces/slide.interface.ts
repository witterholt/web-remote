export interface Slide {
  selected: boolean;
  html: string;
  tag: string;
  text: string;
  chords: string;
  lines: string[];
  first_slide_of_tag: boolean;
  img: string;
}
