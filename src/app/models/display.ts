import { DisplayMode } from '../enums/display-mode.enum';

export class Display {
  bigDisplayButtons: boolean;
  darkTheme: boolean;
  displayMode: DisplayMode;
}
