export enum DisplayMode {
  Blank,
  Theme,
  Desktop,
  Presentation
}
