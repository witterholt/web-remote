export interface Shortcut {
  action: string;
  shortcut: string[];
}
