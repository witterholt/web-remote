export interface SystemInformation {
  websocket_port: number;
  login_required: boolean;
  api_version?: number;
  api_revision?: number;
}
