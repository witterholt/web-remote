export interface ServiceItem {
  id: string;
  notes: string;
  plugin: string;
  selected: boolean;
  title: string;
  is_valid: boolean;
  slides: object[];
}
