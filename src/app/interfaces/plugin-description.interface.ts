export interface PluginDescription {
  key: string;
  name: string;
}
